import { connect } from 'react-redux'
import { addMovie } from '../actions/movie'
import addMovieDialog from '../components/addMovieDialog'


const mapStateToProps = (state) => ({

})
export default connect(
  mapStateToProps,
  { addMovie }
)(addMovieDialog)
