import { connect } from 'react-redux'
import MyCalendar from './../components/Calendar'
import { fetchMovieInformation } from './../actions/movie'

const mapStateToProps = (state) => ({
  events: state.selectedCinemaSchedule,
})
export default connect(
  mapStateToProps,
  {fetchMovieInformation}
)(MyCalendar)
