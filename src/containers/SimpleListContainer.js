import { connect } from 'react-redux'
import { selectCinema, fetchCinemas } from '../actions/cinema'
import SimpleList from '../components/SimpleList'


const mapStateToProps = (state) => ({
  cinemas: state.cinemas
})
export default connect(
  mapStateToProps,
  { selectCinema }
)(SimpleList)
