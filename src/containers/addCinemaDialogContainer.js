import { connect } from 'react-redux'
import { addCinema } from '../actions/cinema'
import addCinemaDialog from '../components/addCinemaDialog'


const mapStateToProps = (state) => ({

})
export default connect(
  mapStateToProps,
  { addCinema }
)(addCinemaDialog)
