import { connect } from 'react-redux'
import { addEvent } from '../actions/schedule'
import AddEventDialog from '../components/addEventDialog'

const mapStateToProps = (state) => ({
  movies: state.movies,
})
export default connect(
  mapStateToProps,
  { addEvent }
)(AddEventDialog)
