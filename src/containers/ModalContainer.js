import { connect } from 'react-redux'
import ScrollDialog from './../components/Modal'
import { fetchMovieInformation } from './../actions/movie'

const mapStateToProps = (state) => ({
  movie: state.selectedMovie,
})
export default connect(
  mapStateToProps,
  {}
)(ScrollDialog)
