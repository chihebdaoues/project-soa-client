import {fetchCinemaSchedule} from './schedule'

export const SELECT_CINEMA = 'SELECT_CINEMA'
export function selectCinema(cinemaId){
  return function(dispatch){
      dispatch({
        type: SELECT_CINEMA,
        cinemaId: cinemaId,
      })
      dispatch(fetchCinemaSchedule(cinemaId))
  }
}

export const GET_CINEMAS = 'GET_CINEMAS'
function receiveCinemas(payload) {
  return {
    type: GET_CINEMAS,
    cinemas: payload.cinemaInfo
  }
}
export function fetchCinemas() {
  return function(dispatch, getState) {
    getState().clients.cinemas.getAllCinemas({}, (err, result) => {
      if(result){
        dispatch(receiveCinemas(result))
      }
    })
  }
}

export function addCinema(cinema) {
  return function(dispatch, getState) {
    getState().clients.cinemas.addCinema(cinema, (err, result) => {
      if(result){
        dispatch(fetchCinemas())
      }
    })
  }
}
