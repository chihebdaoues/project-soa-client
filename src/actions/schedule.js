export const GET_CINEMA_SCHEDULE = 'GET_CINEMA_SCHEDULE'
function receiveCinemaSchedule(cinemaId) {
  return {
    type: GET_CINEMA_SCHEDULE,
    schedule: [],
    cinemaId: cinemaId,
  }
}
export function fetchCinemaSchedule(cinemaId) {
  return function(dispatch) {
    dispatch(receiveCinemaSchedule(cinemaId))
  }
}

export function addEvent(event) {
  return function(dispatch, getState) {
    getState().clients.cinemaSchedule.addMovieToCinema(event, (err, result) => {
      if(result)
        dispatch(fetchCinemaSchedule(getState().selectedCinema))
    })
  }
}
