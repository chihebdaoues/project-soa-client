export function addMovie(movie) {
  return function(dispatch, getState) {
    getState().clients.movies.addMovie(movie, (err, result) => {
      if(result)
        dispatch(fetchMovies())
    })
  }
}

export const GET_MOVIES = 'GET_MOVIES'
function receiveMovies(payload) {
  console.log(payload);
  return {
    type: GET_MOVIES,
    movies: payload.movieInfo
  }
}
export function fetchMovies() {
  return function(dispatch, getState) {
    getState().clients.movies.getAllMovies({}, (err, result) => {
      console.log(result);
      if(result){
        dispatch(receiveMovies(result))
      }
    })
  }
}

export const GET_MOVIE_INFORMATION = 'GET_MOVIE_INFORMATION'
function receiveMovieInformation(payload) {
  return {
    type: GET_MOVIE_INFORMATION,
    payload
  }
}
export function fetchMovieInformation(movieTitle) {
  return function(dispatch) {
    let url = 'http://www.omdbapi.com/?http://www.omdbapi.com/?i=tt3896198&apikey=bf41412b&t='+'Titanic'
    return fetch(url)
      .then(
        response => response.json(),
        error => console.log('An error occurred.', error)
      )
      .then(json =>
        dispatch(receiveMovieInformation(json))
      )
  }
}
