import { fetchCinemas } from './cinema'
import { fetchMovies } from './movie'
var soap = require('soap');

export const SETUP_CLIENTS = 'SETUP_CLIENTS'
function clientCreated(client, clientName) {
  return {
    type: SETUP_CLIENTS,
    client: client,
    clientName: clientName
  }
}

export function setupClients() {
  return function(dispatch) {
    var cinemaWsdlUrl = 'http://localhost:8080/soapws/cinemas.wsdl';
    var movieWsdlUrl = 'http://localhost:8080/soapws/movies.wsdl';
    var cinemaMovieWsdlUrl = 'http://localhost:8080/soapws/cinemamovie.wsdl';

    soap.createClientAsync(cinemaWsdlUrl).then((client) => {
      dispatch(clientCreated(client, 'cinemas'))
      dispatch(fetchCinemas())
    });

    soap.createClientAsync(movieWsdlUrl).then((client) => {
      dispatch(clientCreated(client, 'movies'))
      dispatch(fetchMovies())
    });

    soap.createClientAsync(cinemaMovieWsdlUrl).then((client) => {
      dispatch(clientCreated(client, 'cinemaSchedule'))
    });
  }
}
