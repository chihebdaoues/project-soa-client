import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import AddMovieDialog from './../containers/addMovieDialogContainer';
import SimpleList from './../containers/SimpleListContainer';
import MyCalendar from './../containers/CalendarContainer'
var soap = require('soap');

const styles = theme => ({
  root: {
    flexGrow: 1,
    margin: 16
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
});

class App extends Component {

  render(){
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Grid container spacing={24}>
          <Grid item xs={4}>
            <Paper className={classes.paper}>
              <SimpleList />
            </Paper>
            <AddMovieDialog />

          </Grid>
          <Grid item xs={8}>
            <Paper className={classes.paper}>
              <MyCalendar />
            </Paper>
          </Grid>

        </Grid>
      </div>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(App);
