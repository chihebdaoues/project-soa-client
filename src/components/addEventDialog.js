import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import MenuItem from '@material-ui/core/MenuItem';


class AddEventDialog extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      open: false,
      movieId: 0
    };
  }

  handleClickOpen = () => {
    this.setState({open: true});
  };

  handleClose = (event) => {
    this.setState({ open: false });
  };

  handleChange = name => event => {
    this.setState({ [name]: event.target.value});
  };

  handleSave = (event) => {
    let eventDate = this.newEventDate.value;
    let eventStartTime = this.newEventStartTime.value;
    let eventEndTime = this.newEventEndTime.value;

    let eventStart = new Date(eventDate + " " + eventStartTime)
    let eventEnd = new Date(eventDate + " " + eventEndTime)
    this.props.addEvent({movieId: this.state.movieId, dateD: ""+eventDate, dateF: ""+eventDate})
    this.setState({ open: false });
  };

  render() {
    return (
      <div>
        <Button onClick={this.handleClickOpen}>Add event</Button>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Add event</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Adding An Event:
            </DialogContentText>
            <TextField
              id="movieId"
              select
              label="Select Movie"
              value={this.state.movieId}
              onChange={this.handleChange('movieId')}
              style={{ width: '100%' }}
            >
              {this.props.movies.map(option => (
                <MenuItem key={option.movieId} value={option.movieId}>
                  {option.title}
                </MenuItem>
              ))}
            </TextField>
            <TextField
              autoFocus
              margin="dense"
              id="eventDate"
              label="Movie Starting Date"
              type="date"
              InputLabelProps={{ shrink: true }}
              fullWidth
              inputRef={(input) => {this.newEventDate = input }}
            />
            <TextField
              autoFocus
              margin="dense"
              id="eventStartTime"
              label="Movie Starting Time"
              type="time"
              InputLabelProps={{ shrink: true }}
              fullWidth
              inputRef={(input) => {this.newEventStartTime = input }}
            />
            <TextField
              autoFocus
              margin="dense"
              id="eventEndTime"
              label="Movie Ending Time"
              type="time"
              InputLabelProps={{ shrink: true }}
              fullWidth
              inputRef={(input) => {this.newEventEndTime = input }}
            />

          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleSave} color="primary">
              Save
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default AddEventDialog;
