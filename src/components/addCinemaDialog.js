import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

class AddCinemaDialog extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      open: false
    };
  }

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = (event) => {
    this.setState({ open: false });
  };

  handleSave = (event) => {
    let cinemaTitle = this.newCinemaTitle.value;
    let cinemaAddress = this.newCinemaAddress.value;
    this.props.addCinema({title: cinemaTitle, address: cinemaAddress});
    this.setState({ open: false });
  };

  render() {
    return (
      <div>
        <Button onClick={this.handleClickOpen}>Add Cinema</Button>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Add Cinema</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Adding A Cinema:
            </DialogContentText>
            <TextField
              autoFocus
              margin="dense"
              id="title"
              label="Cinema Name"
              fullWidth
              inputRef={(input) => {this.newCinemaTitle = input }}
            />
            <TextField
              margin="dense"
              id="address"
              label="Cinema Address"
              inputRef={(input) => {this.newCinemaAddress = input }}
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleSave} color="primary">
              Save
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default AddCinemaDialog;
