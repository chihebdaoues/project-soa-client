import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

class AddMovieDialog extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      open: false,
    };
  }

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = (event) => {
    this.setState({ open: false });
  };

  handleSave = (event) => {
    let movietitle = this.newMovieTitle.value;
    let movieDateProduction = this.newMovieDateProduction.value;
    this.props.addMovie({title: movietitle, dateProduction: movieDateProduction})
    this.setState({ open: false });
  };

  render() {
    return (
      <div>
        <Button onClick={this.handleClickOpen}>Add Movie</Button>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Add Movie</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Adding A Movie:
            </DialogContentText>
            <TextField
              margin="dense"
              id="address"
              label="Movie title"
              inputRef={(input) => {this.newMovieTitle = input }}
              fullWidth
            />
            <TextField
              autoFocus
              margin="dense"
              id="title"
              label="Movie Production Date"
              type="date"
              fullWidth
              InputLabelProps={{ shrink: true }}
              inputRef={(input) => {this.newMovieDateProduction = input }}
            />

          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleSave} color="primary">
              Save
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default AddMovieDialog;
