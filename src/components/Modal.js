import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Typography from '@material-ui/core/Typography';

class ScrollDialog extends React.Component {
  render() {
    let content = null;
    if (this.props.movie){
      let movie = this.props.movie;
      content = (<div>
        <Dialog
          open={this.props.open}
          onClose={this.props.onClose}
          scroll='paper'
          aria-labelledby="scroll-dialog-title"
        >
          <DialogTitle id="scroll-dialog-title">
            {movie.Title+" ("+movie.Year+")"}
          </DialogTitle>
          <DialogContent>
            <Typography variant="subtitle1" gutterBottom>
              Duration: {movie.Runtime}, Genre: {movie.Genre}, Rated: {movie.Rated}
            </Typography>
            <img alt="poster" src={movie.Poster} style={{margin: 'auto', display: 'block'}}/>
            <DialogContentText>
              {movie.Plot}
              <br />
              Actors: {movie.Actors}
            </DialogContentText>
          </DialogContent>
        </Dialog>
      </div>)
    }
    return content;
  }
}

export default ScrollDialog;
