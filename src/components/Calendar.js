import React from 'react';
import BigCalendar from 'react-big-calendar'
import moment from 'moment'
import 'react-big-calendar/lib/css/react-big-calendar.css'
import ScrollDialog from '../containers/ModalContainer'
import AddEventDialog from './../containers/addEventDialogContainer'

class MyCalendar extends React.Component{
  state = {
    open: false,
    title: 'none'
  }

  handleClose = () => {
    this.setState({ open: false });
  };

  render(){
    const localizer = BigCalendar.momentLocalizer(moment)
    let calendar = null;
    calendar = (
        <div>
          <AddEventDialog />
          <BigCalendar
            localizer={localizer}
            events={this.props.events}
            startAccessor="start"
            endAccessor="end"
            defaultView="week"
            onSelectEvent={(event: Object, e: SyntheticEvent) => {this.setState({open: true}); this.props.fetchMovieInformation(event.title)}}
          />
        </div>);
    return (
      <div>
        {calendar}
        <ScrollDialog open={this.state.open} onClose={this.handleClose} />
      </div>)
  }
}
export default MyCalendar;
