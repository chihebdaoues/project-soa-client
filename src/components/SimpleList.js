import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import InboxIcon from '@material-ui/icons/Inbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import AddCinemaDialog from './../containers/addCinemaDialogContainer';

const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
});

function ListItemLink(props) {
  return <ListItem button component="a" {...props} />;
}

class SimpleList extends React.Component {
  state = {
    open: false,
  };

  render(){
    const { classes } = this.props;
    let list = null;
    if(this.props.cinemas){
      list = [];
      this.props.cinemas.map((obj)=>list.push(<ListItem button key={obj.cinemaId} onClick={()=>this.props.selectCinema(obj.cinemaId)}>
        <ListItemIcon>
          <InboxIcon />
        </ListItemIcon>
        <ListItemText primary={obj.title} secondary={obj.address} />
      </ListItem>))
    }
    return (
      <div className={classes.root}>
        <List component="nav">
          {list}
        </List>
        <AddCinemaDialog />
      </div>
    );
  }
}

SimpleList.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SimpleList);
