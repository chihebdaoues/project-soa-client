import thunkMiddleware from 'redux-thunk'
import { createLogger } from 'redux-logger'
import { createStore, applyMiddleware, compose } from 'redux'
import { setupClients } from '../actions'
import rootReducer from '../reducers'

const loggerMiddleware = createLogger()
let initialState = {
  clients: {cinemas: null,},
  cinemas: [],
  movies: [],
  selectedCinema: 0,
  selectedCinemaSchedule: [{
    id: 0,
    title: 'Titanic',
    start: Date.now(),
    end: Date.now()-3,
  }],
  selectedMovie:{
    title: null,
    info: null
  }
}
export const store = createStore(
  rootReducer,
  initialState,
  compose(
    applyMiddleware(
      thunkMiddleware,
      loggerMiddleware
    ),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  )
)

// store.dispatch(fetchCinemas()).then(() => console.log(store.getState()))
// store.dispatch(fetchMovies()).then(() => console.log(store.getState()))
store.dispatch(setupClients())
